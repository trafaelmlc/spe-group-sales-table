package br.com.fastshop.resource;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import br.com.fastshop.dto.GroupSalesTable;
import br.com.fastshop.service.GroupSalesTableService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@RequestMapping("/spe-group-sales-table")
public class GroupSalesTableResources {

	@Autowired
	private GroupSalesTableService service;

	@PostMapping("/")
	public ResponseEntity<GroupSalesTable> insert(@RequestBody GroupSalesTable tabvendas) {
		return service.create(tabvendas);
	}

	@PutMapping("/")
	public ResponseEntity<GroupSalesTable> update(@RequestBody GroupSalesTable tabvendas) {
		return service.update(tabvendas);
	}

	@ApiOperation(value = "Get Group sales by id")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Operation performed successfully"),
			@ApiResponse(code = 401, message = "Unauthorized to view"),
			@ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
			@ApiResponse(code = 404, message = "The resource you were trying to reach is not found") })
	@GetMapping("/{id}")
	public ResponseEntity<GroupSalesTable> getById(@PathVariable("id") String id) {
		return service.getById(id);
	}

	@GetMapping
	public ResponseEntity<List<GroupSalesTable>> getAll() {
		return service.getAll();
	}

	@DeleteMapping("/{id}")
	public Boolean deleteById(@PathVariable String id) {
		return service.deleteById(id);
	}

	@GetMapping("/findByName/{name}")
	public ResponseEntity<GroupSalesTable> findByName(@RequestParam String name) {
		return service.findByName(name);
	}
}
