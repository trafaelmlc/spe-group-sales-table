package br.com.fastshop.proxy;

import java.io.IOException;
import java.util.List;

import org.springframework.cloud.netflix.ribbon.RibbonClient;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import br.com.fastshop.dto.GroupSalesTable;
import feign.FeignException;

@FeignClient(url = "${fastshop.url.rs-group-sales-table}", name = "rs-group-sales-table/api")
@RibbonClient(name = "rs-group-sales-table/api")
public interface ProxyGroupSalesTable {

	@PostMapping("/")
	ResponseEntity<GroupSalesTable> insert(@RequestBody GroupSalesTable grupoSales) throws IOException, FeignException;

	@PutMapping("/")
	ResponseEntity<GroupSalesTable> save(@RequestBody GroupSalesTable grupoSales) throws IOException, FeignException;

	@GetMapping("/{id}")
	public ResponseEntity<GroupSalesTable> getById(@PathVariable String id) throws IOException, FeignException;

	@GetMapping("/")
	ResponseEntity<List<GroupSalesTable>> getAll() throws IOException, FeignException;

	@DeleteMapping("/{id}")
	Boolean deleteById(@PathVariable String id) throws IOException, FeignException;
	
	@GetMapping("/findByName/{name}")
	public ResponseEntity<GroupSalesTable> findByName(@RequestParam String name) throws IOException, FeignException;

}
