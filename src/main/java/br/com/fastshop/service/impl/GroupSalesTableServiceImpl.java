package br.com.fastshop.service.impl;

import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.GetMapping;

import br.com.fastshop.dto.GroupSalesTable;
import br.com.fastshop.proxy.ProxyGroupSalesTable;
import br.com.fastshop.service.GroupSalesTableService;
import feign.FeignException;
import lombok.extern.log4j.Log4j2;

@Service
@Log4j2
public class GroupSalesTableServiceImpl implements GroupSalesTableService {

	@Autowired
	private ProxyGroupSalesTable proxy;

	@Override
	public ResponseEntity<GroupSalesTable> create(GroupSalesTable grupoSale) {
		log.info("start insert group sale");
		ResponseEntity<GroupSalesTable> dto = null;
		try {
			dto = proxy.insert(grupoSale);
		} catch (FeignException e) {
			if (e.status() == 409) {
				log.error("Error:" + e.status());
				return ResponseEntity.status(HttpStatus.CONFLICT).body(null);
			}
		} catch (IOException e) {
			log.error("IOException Error: " + e.getMessage());
			return null;
		}
		return dto;
	}

	@Override
	public ResponseEntity<GroupSalesTable> update(GroupSalesTable grupoSale) {
		log.info("start insert group sale");
		ResponseEntity<GroupSalesTable> dto = null;
		try {
			dto = proxy.save(grupoSale);
		} catch (FeignException e) {
			if (e.status() == 409) {
				log.error("Error:" + e.status());
				return ResponseEntity.status(HttpStatus.CONFLICT).body(null);
			}
		} catch (IOException e) {
			log.error("IOException Error: " + e.getMessage());
			return null;
		}
		return dto;
	}

	@Override
	public ResponseEntity<GroupSalesTable> getById(String id) {
		log.info("start find by id group sales table ");
		ResponseEntity<GroupSalesTable> dto = null;
		try {
			return proxy.getById(id);
		} catch (FeignException e) {
			log.error("FeignException Error: " + e.getMessage());
		} catch (IOException e) {
			log.error("IOException Error: " + e.getMessage());
		}
		return dto;
	}

	@Override
	public ResponseEntity<List<GroupSalesTable>> getAll() {
		ResponseEntity<List<GroupSalesTable>> listDtos = null;
		log.info("start get all group sales table ");
		try {
			listDtos = proxy.getAll();
		} catch (FeignException e) {
			log.error("FeignException Error: " + e.getMessage());
		} catch (IOException e) {
			log.error("IOException Error: " + e.getMessage());
		}

		return listDtos;
	}

	public Boolean deleteById(String id) {
		log.info("start delete group sales table ");
		Boolean delete = false;
		try {
			proxy.deleteById(id);
			return delete = true;
		} catch (FeignException e) {
			log.error("FeignException Error: " + e.getMessage());
			return delete;
		} catch (IOException e) {
			log.error("IOException Error: " + e.getMessage());
			return delete;
		}
	}

	@GetMapping("/findByName/{name}")
	public ResponseEntity<GroupSalesTable> findByName(String name) {
		log.info("start find by name group sales table ");
		ResponseEntity<GroupSalesTable> dto = null;
		try {
			return proxy.findByName(name);
		} catch (FeignException e) {
			log.error("FeignException Error: " + e.getMessage());
		} catch (IOException e) {
			log.error("IOException Error: " + e.getMessage());
		}
		return dto;
	}
}
