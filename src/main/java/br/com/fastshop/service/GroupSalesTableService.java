package br.com.fastshop.service;

import java.util.List;

import org.springframework.http.ResponseEntity;

import br.com.fastshop.dto.GroupSalesTable;

public interface GroupSalesTableService {

	ResponseEntity<GroupSalesTable> create(GroupSalesTable sale);

	ResponseEntity<GroupSalesTable> update(GroupSalesTable sale);

	ResponseEntity<GroupSalesTable> getById(String id);

	ResponseEntity<List<GroupSalesTable>> getAll();

	Boolean deleteById(String id);

	ResponseEntity<GroupSalesTable> findByName(String name);
}
