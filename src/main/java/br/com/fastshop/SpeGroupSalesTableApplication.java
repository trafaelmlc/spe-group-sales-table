package br.com.fastshop;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableFeignClients
public class SpeGroupSalesTableApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpeGroupSalesTableApplication.class, args);
	}
}
