package br.com.fastshop.dto;

import java.time.LocalDateTime;
import java.util.List;

import lombok.Data;

@Data
public class GroupSalesTable {
	
	private String id;

	private String name;

	private String description;

	private String createdBy;

	private LocalDateTime createdDate;

	private String updatedBy;

	private LocalDateTime updatedDate;

	private List<String> payment_group_sales_table;

}
